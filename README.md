# Introduction #

**This readme is adapted for the various branches**

With the help of the scripts provided in this repository, you are able to compile all necessary prerequisites to use the H.E.S.S. software, libCHEC or the CHEC DACQ board firmware & gateware.

There are several different branches available for different operating systems and software installations:

 * **fc12**      -> Branch to install the all of the H.E.S.S. software on a Fedora Core 12 system using an up to date compiler and python. Note that you have to install missing dependencies via yum.
 * **fc6_hess**  -> Branch to install all of the H.E.S.S. software on a Scientific Linux 6 system using an up to date compiler and python. Note that you have to install missing dependencies via yum.
 * **fc7_hess**  -> Branch to install all of the H.E.S.S. software on a Scientific Linux 7 system using an up to date compiler and python. Please check the SL7 setup guide.

Please look for detailed instructions for each branch below!

# General Instructions #

 * Execute **git clone https://bitbucket.org/ArnimBalzer/bashcompilescripts.git** to download the repository
 * To checkout a given branch, enter the repository and execute *git checkout BRANCHNAME*
 * The structure of all branches is the same: One main **Install.sh** script, an **Eclipse.sh** script, a **scripts** directory, a **patch** directory and either a **Hess.sh** or **Dacq.sh**/**Chec.sh** file
 * To enable or disable certain libraries or tools, just open the **Install.sh** script and (un)comment the respective line(s)
 * Source files will be downloaded to the **source** directory in the repository
 * Log files for each item in the **Install.sh** script will be create in the **logs** directory in the repository
 * After each successful installation for every item in the **Install.sh** script, a file will be touched in the **.done** directory in the repository. Remove this file to force a recompilation of a component
 * Each main script has a **help output** that can be accessed via **-h** or **--help**, use it!
 * Main scripts will perform parameter checking and not execute without all necessary parameters specified! This doe not apply to any script in the **scripts** directory!
 * The main scripts can be called from any directory!
 * You can choose between a module based setup or a bash init script based setup. However, you can't mix them, its either or! (Unless you are willing to do things by hand)
 
**!!!Please check the actual scripts in case you want to know what they are doing!!!**