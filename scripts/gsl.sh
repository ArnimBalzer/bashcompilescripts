#!/bin/bash
set -e 
set -o pipefail

source `dirname ${0}`/Helper.sh

FILE=$(DownloadFile http://ftp.snt.utwente.nl/pub/software/gnu/gsl/gsl-latest.tar.gz)
ExtractTo ${FILE} ${BUILDDIR}

./configure --prefix=${PREFIXDIR} --with-pic
make -j$(GetNProc)
make install
