#!/bin/bash
set -e 
set -o pipefail

source `dirname ${0}`/Helper.sh

BackupFile ~/.emacs

cat > ~/.emacs << EOF
;; .emacs

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(make-backup-files nil)
 '(auto-save-default nil)
 '(diff-switches "-u")
 '(inhibit-startup-screen t))

;;; uncomment for CJK utf-8 support for non-Asian users
;; (require 'un-define)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
EOF
