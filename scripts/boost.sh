#!/bin/bash
set -e 
set -o pipefail

source `dirname ${0}`/Helper.sh

FILE=$(DownloadFile http://sourceforge.net/projects/boost/files/latest/download?source=files) 
ExtractTo ${FILE} ${BUILDDIR}

./bootstrap.sh --with-toolset=${COMPILER} --with-icu --prefix=${PREFIXDIR}
./b2 -j$(GetNProc)
./b2 install

cd ${PREFIXDIR}
patch -p0 < ${PATCHDIR}/boost.patch

mkdir -p ${PREFIXDIR}/lib/pkgconfig

cat > ${PREFIXDIR}/lib/pkgconfig/boost.pc << EOF
prefix=${PREFIXDIR}
exec_prefix=${PREFIXDIR}
libdir=${PREFIXDIR}/lib
includedir=${PREFIXDIR}/include

Name: BOOST
Description: BOOST C++ library
Version: 1.58.0
Libs: -L${PREFIXDIR}/lib
Cflags: -I${PREFIXDIR}/include
EOF
