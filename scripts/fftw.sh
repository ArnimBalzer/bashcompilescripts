#!/bin/bash
set -e 
set -o pipefail

source `dirname ${0}`/Helper.sh

FILE=$(DownloadFile http://www.fftw.org/fftw-3.3.4.tar.gz)
ExtractTo ${FILE} ${BUILDDIR}

./configure --prefix=${PREFIXDIR} --with-pic --enable-threads --enable-sse2 --enable-avx --enable-shared
make -j$(GetNProc)
make install
