#!/bin/bash
set -e 
set -o pipefail

source `dirname ${0}`/Helper.sh

sudo yum -y update --skip-broken

sudo yum -y group install \
	"Development and Creative Workstation" \
	"Compatibility Libraries" \
	"Development Tools" \
	"Legacy UNIX Compatibility" \
    "Graphical Administration Tools" \
    "System Administration Tools"

sudo yum -y install epel-release.noarch

sudo rpm --force -Uvh http://dev.mysql.com/get/mysql-community-release-el7-5.noarch.rpm

sudo yum -y install htop.x86_64 iftop.x86_64 tcl-devel.x86_64 tclx-devel.x86_64 webkitgtk.x86_64 libwebkit2gtk.x86_64 glibc-devel.i686 libgcc.i686 \
    mysql++.x86_64 mysql++-devel.x86_64 mysql-workbench-community.x86_64 mysql-community-client.x86_64 hdf5.x86_64 hdf5-devel.x86_64 \
    lapack-devel.x86_64 blas-devel.x86_64 libpcap.x86_64 libpcap-devel.x86_64 lua.x86_64 lua-devel.x86_64 openssl-devel.x86_64 boost.x86_64 \
    qt.x86_64 qt-devel.x86_64 qt-x11.x86_64 libevent-devel.x86_64 giflib-devel.x86_64 graphviz-devel.x86_64 pygtk2-devel.x86_64 \
    glade-devel.x86_64 libglade2-devel.x86_64 gtk+.x86_64 qt-creator.x86_64 pcre-devel.x86_64 cvs.x86_64 libzip.i686 libzip.x86_64 \
    libzip-devel.x86_64 libzip-devel.i686 fakeroot.x86_64 glibc-static.i686 glibc-static.x86_64 texinfo libXpm-devel.x86_64 \
    tftp.x86_64 tftp-server.x86_64 dhcp.x86_64 x11vnc.x86_64 tigervnc-server.x86_64 iperf.x86_64 iperf3.x86_64
