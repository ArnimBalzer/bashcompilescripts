#!/bin/bash
set -e 
set -o pipefail

source `dirname ${0}`/Helper.sh

FILE=$(DownloadFile http://valgrind.org/downloads/valgrind-3.10.1.tar.bz2)
ExtractTo ${FILE} ${BUILDDIR}

./configure --prefix=${PREFIXDIR}
make -j$(GetNProc)
make install
