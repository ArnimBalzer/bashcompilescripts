#!/bin/bash
set -e 
set -o pipefail

source `dirname ${0}`/Helper.sh

FILE=$(DownloadFile http://nmap.org/dist/nmap-6.49BETA3.tar.bz2)
ExtractTo ${FILE} ${BUILDDIR}

./configure --prefix=${PREFIXDIR}
make -j$(GetNProc)
make install
