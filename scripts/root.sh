#!/bin/bash
set -e 
set -o pipefail

source `dirname ${0}`/Helper.sh

FILE=$(DownloadFile http://root.cern.ch/download/root_v5.34.32.source.tar.gz)
ExtractTo ${FILE} ${BUILDDIR}/root

mkdir -p ${BUILDDIR}/build
cd ${BUILDDIR}/build

cmake ../root/ -DCMAKE_INSTALL_PREFIX=${PREFIXDIR} -DCMAKE_BUILD_TYPE=Release -Dall=ON -Wno-dev
make -j$(GetNProc)
make install
