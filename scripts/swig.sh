#!/bin/bash
set -e 
set -o pipefail

source `dirname ${0}`/Helper.sh

FILE=$(DownloadFile http://prdownloads.sourceforge.net/swig/swig-3.0.5.tar.gz)
ExtractTo ${FILE} ${BUILDDIR}

./configure --prefix=${PREFIXDIR} --enable-cpp11-testing --with-boost=${PREFIXDIR}
make -j$(GetNProc)
make install
