#!/bin/bash
set -e 
set -o pipefail

source `dirname ${0}`/Helper.sh

FILE=$(DownloadFile http://cairographics.org/releases/pycairo-1.8.8.tar.gz)
ExtractTo ${FILE} ${BUILDDIR}

./configure --prefix=${PREFIXDIR}
make -j$(GetNProc)
make install
