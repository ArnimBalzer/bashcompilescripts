#!/bin/bash

GetDoneFile() {
    local NAME=${1}
    echo ${SISDIR}/.done/${NAME}
}

TestDone() {
    local NAME=${1}
    rm -rf ${BUILDDIR}
    mkdir -p ${BUILDDIR}
    if [ -f $(GetDoneFile ${NAME}) ]; then
		return 0
	else
    	return 1
    fi
}

ExecuteAndPipe() {
    local NAME=${1}
    shift
    mkdir -p ${LOGDIR}
    local LOGFILE="${LOGDIR}/${NAME}.log"
    if $(TestDone ${NAME}); then
		echo "Skipping ${NAME}.sh, already done"
	else
    	echo "Excecuting ${NAME}.sh"
    	rm -f ${LOGFILE}
    	${SCRIPTDIR}/${NAME}.sh $@ 2>&1 | tee -a ${LOGFILE}
    	SetDone ${NAME}
	fi
}

SetDone() {
    local NAME=${1}
    rm -rf ${BUILDDIR}
    local DONE=$(GetDoneFile ${NAME})
    mkdir -p `dirname ${DONE}`
    touch ${DONE}
    cd ${SCRIPTSDIR}
}

BackupFile() {
	local FILE=${1}
	if [ -f ${FILE} ]; then
		cp --backup=numbered ${FILE} ${FILE}_back
	fi
}

DownloadFile() {
	local URL=${1}
	local FILE=`curl -sIL ${URL} | perl -n -e'/filename="(.*)"/ && print $1'`
	if [ "x${FILE}" == "x" ]; then
		FILE=`basename ${1}`
	fi
	mkdir -p ${SOURCEDIR}
	if [ ! -f ${SOURCEDIR}/${FILE} ]; then
		curl -#L ${URL} -o ${SOURCEDIR}/${FILE} 
	fi
	echo ${FILE}
}

ExtractTo() {
    local ARCHIVE=${1}
    local TARGET=${2}
    mkdir -p ${TARGET}
    local OPTION="xf"
    if [ "${ARCHIVE##*.}" == "xgz" ]; then OPTION="${OPTION}z"; fi
    if [ "${ARCHIVE###*.}" == "xbz2" ]; then OPTION="${OPTION}j"; fi
    if [ "${ARCHIVE##*.}" == "xxz" ]; then OPTION="${OPTION}J"; fi
    tar xf ${SOURCEDIR}/${ARCHIVE} -C ${TARGET} --strip-components=1
    cd ${TARGET}
}

GetNProc() {
    echo `cat /proc/cpuinfo| grep processor| wc -l`
}
