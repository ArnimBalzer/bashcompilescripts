#!/bin/bash
set -e 
set -o pipefail

source `dirname ${0}`/Helper.sh

FILE=$(DownloadFile http://valgrind.org/downloads/valkyrie-2.0.0.tar.bz2)
ExtractTo ${FILE} ${BUILDDIR}

for FILE in "src/objects/tool_object.cpp" "src/utils/vk_config.cpp" "src/utils/vk_utils.cpp"; do
    echo "#include <unistd.h>" | cat - ${FILE} > /tmp/out && mv /tmp/out ${FILE}
done

qmake-qt4 PREFIX=${PREFIXDIR}
make -j$(GetNProc)
make install
