#!/bin/bash
set -e 
set -o pipefail

source `dirname ${0}`/Helper.sh

FILE=$(DownloadFile http://llvm.org/releases/3.6.1/llvm-3.6.1.src.tar.xz)
ExtractTo ${FILE} ${BUILDDIR}/llvm

FILE=$(DownloadFile http://llvm.org/releases/3.6.1/cfe-3.6.1.src.tar.xz)
ExtractTo ${FILE{ ${BUILDDIR}/llvm/tools/clang

FILE=$(DownloadFile http://llvm.org/releases/3.6.1/clang-tools-extra-3.6.1.src.tar.xz)
ExtractTo ${FILE} ${BUILDDIR}/llvm/tools/clang/tools/extra

FILE=$(DownloadFile http://llvm.org/releases/3.6.1/compiler-rt-3.6.1.src.tar.xz)
ExtractTo ${FILE} ${BUILDDIR}/llvm/projects/compiler-rt

mkdir -p ${BUILDDIR}/build
cd ${BUILDDIR}/build

cmake ../llvm/ -DCMAKE_INSTALL_PREFIX=${PREFIXDIR} -DCMAKE_BUILD_TYPE="Release" -DLLVM_ENABLE_ASSERTIONS=OFF -DBUILD_SHARED_LIBS=OFF
make -j$(GetNProc)
make install

export CC=${PREFIXDIR}/bin/clang
export CXX=${PREFIXDIR}/bin/clang++