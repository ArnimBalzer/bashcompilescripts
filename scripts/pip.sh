#!/bin/bash
set -e 
set -o pipefail

source `dirname ${0}`/Helper.sh

FILE=$(DownloadFile https://bootstrap.pypa.io/get-pip.py)
python ${SOURCEDIR}/get-pip.py

pip install pip-tools
pip install argparse
pip install Cython
pip install ipython
pip install numpy
pip install scipy
pip install colorama
#pip install matplotlib
#pip install astropy
pip install protobuf==2.6.1
pip install pyzmq
