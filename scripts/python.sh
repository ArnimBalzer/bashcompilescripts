#!/bin/bash
set -e 
set -o pipefail

source `dirname ${0}`/Helper.sh

FILE=$(DownloadFile https://www.python.org/ftp/python/2.7.10/Python-2.7.10.tar.xz) 
ExtractTo ${FILE} ${BUILDDIR}

./configure --prefix=${PREFIXDIR} --enable-shared --enable-unicode=ucs2
make -j$(GetNProc)
make install
