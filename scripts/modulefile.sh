#!/bin/bash
set -e 
set -o pipefail

source `dirname ${0}`/Helper.sh

mkdir -p ~/modules

BackupFile ~/modules/${MODULENAME}

cat > ~/modules/${MODULENAME} << EOF
#%Module
#author: arnim.balzer@uva.nl
set pkgName ${MODULENAME}
set pkgHome ${PREFIXDIR:0:${#PREFIXDIR}-1}

proc ModulesHelp { } {
    global pkgName
    global pkgHome
    puts stderr "\tSets up the environment for \$pkgName located in \$pkgHome\n"
    puts stderr "\t- PATH"
    puts stderr "\t- LD_LIBRARY_PATH"
    puts stderr "\t- LD_RUN_PATH"
    puts stderr "\t- PKG_CONFIG_PATH"
    puts stderr "\t- PYTHONPATH"
    puts stderr "\t- MANPATH"
    puts stderr "\t- CMAKE_PREFIX_PATH\n"
}

module-whatis "sets up the environment for \$pkgName located in \$pkgHome"

conflict \$pkgName

setenv CC ${CC}
setenv CXX ${CXX}

setenv SWT_GTK3 0

setenv ROOTSYS \$pkgHome

prepend-path PATH               \$pkgHome/bin
prepend-path LD_LIBRARY_PATH    \$pkgHome/lib
prepend-path LD_LIBRARY_PATH    \$pkgHome/lib64
prepend-path LD_RUN_PATH        \$pkgHome/lib
prepend-path LD_RUN_PATH        \$pkgHome/lib64
prepend-path PKG_CONFIG_PATH    \$pkgHome/lib/pkgconfig
prepend-path PKG_CONFIG_PATH    \$pkgHome/lib64/pkgconfig
prepend-path PYTHONPATH         \$pkgHome/lib
prepend-path PYTHONPATH         \$pkgHome/lib64/python2.7/site-packages
prepend-path MANPATH            \$pkgHome/share/man
prepend-path MANPATH            \$pkgHome/man
prepend-path PFILES             \$pkgHome/pfiles
prepend-path CMAKE_PREFIX_PATH  \$pkgHome

EOF
