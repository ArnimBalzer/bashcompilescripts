#!/bin/bash
set -e 
set -o pipefail

source `dirname ${0}`/Helper.sh

FILE=$(DownloadFile http://ftp.gnu.org/gnu/gdb/gdb-7.9.1.tar.xz)
ExtractTo ${FILE} ${BUILDDIR}

export LDFLAGS="-L${PREFIXDIR}/lib"
./configure --prefix=${PREFIXDIR}
make -j$(GetNProc)
make install
unset LDFLAGS
