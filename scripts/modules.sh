#!/bin/bash
set -e 
set -o pipefail

source `dirname ${0}`/Helper.sh

FILE=$(DownloadFile http://sourceforge.net/projects/modules/files/latest/download?source=files)
ExtractTo ${FILE} ${BUILDDIR}

./configure --prefix=${PREFIXDIR} --with-tclx-lib=/usr/lib64/tcl8.5/tclx8.4 --with-tclx-inc=/usr/include --with-tclx-ver=8.4 --enable-versioning=no
make -j$(GetNProc)
make install

cat >> ~/.bashrc << EOF

export MODULEPATH=~/modules/
source ${PREFIXPATH}/Modules/init/bash
EOF