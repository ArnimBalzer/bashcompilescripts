#!/bin/bash
set -e 
set -o pipefail

source `dirname ${0}`/Helper.sh

FILE=$(DownloadFile http://github.com/farcepest/MySQLdb1/archive/MySQLdb-1.2.5.tar.gz)
ExtractTo ${FILE} ${BUILDDIR}

python setup.py install