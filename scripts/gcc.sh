#!/bin/bash
set -e 
set -o pipefail

source `dirname ${0}`/Helper.sh

FILE=$(DownloadFile http://gcc.cybermirror.org/releases/gcc-4.9.3/gcc-4.9.3.tar.bz2)
ExtractTo ${FILE} ${BUILDDIR}/gcc
./contrib/download_prerequisites

mkdir -p ${BUILDDIR}/build
cd ${BUILDDIR}/build

../gcc/configure --prefix=${PREFIXDIR}
make -j$(GetNProc)
make install

export CC=${PREFIXDIR}/bin/gcc
export CXX=${PREFIXDIR}/bin/g++
