#!/bin/bash
set -e 
set -o pipefail

source `dirname ${0}`/Helper.sh

FILE=$(DownloadFile http://ftp.gnu.org/gnu/bison/bison-3.0.4.tar.xz)
ExtractTo ${FILE} ${BUILDDIR}

./configure --prefix=${PREFIXDIR} --with-pic
make -j$(GetNProc)
make install
