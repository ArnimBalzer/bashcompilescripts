#!/bin/bash
set -e 
set -o pipefail

source `dirname ${0}`/Helper.sh

FILE=$(DownloadFile http://ftp.uni-kl.de/pub/wireshark/src/wireshark-1.12.6.tar.bz2)
ExtractTo ${FILE} ${BUILDDIR}

OLDPATH=${PATH}
PATH=/usr/lib64/qt5/bin:${PATH}

./configure --prefix=${PREFIXDIR} --with-ssl --with-gtk2=yes --with-gtk3=no
make -j$(GetNProc)
make install

PATH=${OLDPATH}