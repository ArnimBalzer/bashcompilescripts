#!/bin/bash
set -e 
set -o pipefail

source `dirname ${0}`/Helper.sh

FILE=$(DownloadFile http://www.cmake.org/files/v3.2/cmake-3.2.3.tar.gz)
ExtractTo ${FILE} ${BUILDDIR}

./bootstrap --prefix=${PREFIXDIR}
make -j$(GetNProc)
make install
