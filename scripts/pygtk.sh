#!/bin/bash
set -e 
set -o pipefail

source `dirname ${0}`/Helper.sh

FILE=$(DownloadFile http://ftp.gnome.org/pub/GNOME/sources/pygtk/2.7/pygtk-2.7.4.tar.bz2)
ExtractTo ${FILE} ${BUILDDIR}

./configure --prefix=${PREFIXDIR}
make -j$(GetNProc)
make install
