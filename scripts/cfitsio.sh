#!/bin/bash
set -e 
set -o pipefail

source `dirname ${0}`/Helper.sh

FILE=$(DownloadFile ftp://heasarc.gsfc.nasa.gov/software/fitsio/c/cfitsio_latest.tar.gz)
ExtractTo ${FILE} ${BUILDDIR}

./configure --prefix=${PREFIXDIR} --enable-reentrant --enable-sse2 --enable-ssse3
make -j$(GetNProc)
make libcfitsio.so
make all
make install
