#!/bin/bash
set -e 
set -o pipefail

source `dirname ${0}`/Helper.sh

BackupFile ~/.bashrc

cat > ~/.bashrc << EOF
# .bashrc

if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

alias ls="ls --color -G"
alias ll="ls --color -Glh"
alias du="du -h"
alias df="df -h"
alias less="less -R"

export LANG="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"

export EDITOR=nano
export HISTSIZE=1000000
export HISTFILESIZE=1000000

EOF
