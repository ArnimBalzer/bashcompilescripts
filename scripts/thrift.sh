#!/bin/bash
set -e 
set -o pipefail

source `dirname ${0}`/Helper.sh

FILE=$(DownloadFile http://apache.cs.uu.nl/thrift/0.9.2/thrift-0.9.2.tar.gz)
ExtractTo ${FILE} ${BUILDDIR}

export PY_PREFIX=${PREFIXDIR}

./configure --prefix=${PREFIXDIR} --with-boost=${PREFIXDIR} --with-lua=no --with-java=no \
			--with-ruby=no --enable-tutorial=no --enable-tests=no --with-php=no --with-cpp=yes
make -j$(GetNProc)
make install

unset PY_PREFIX
