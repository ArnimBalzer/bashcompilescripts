#!/bin/bash
set -e 
set -o pipefail

source `dirname ${0}`/Helper.sh

FILE=$(DownloadFile http://sourceforge.net/projects/omniorb/files/omniORB/omniORB-4.2.0/omniORB-4.2.0.tar.bz2/download)
ExtractTo ${FILE} ${BUILDDIR}

./configure --prefix=${PREFIXDIR}
make -j$(GetNProc)
make install

FILE=$(DownloadFile http://sourceforge.net/projects/omniorb/files/omniORBpy/omniORBpy-4.2.0/omniORBpy-4.2.0.tar.bz2/download)
ExtractTo ${FILE} ${BUILDDIR}

./configure --prefix=${PREFIXDIR} --with-omniorb=${PREFIXDIR}
make -j$(GetNProc)
make install
