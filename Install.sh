#!/bin/bash
set -e 
set -o pipefail

export SISDIR=`readlink -f ${0} | xargs dirname`
export SCRIPTDIR=${SISDIR}/scripts
export SOURCEDIR=${SISDIR}/sources
export PATCHDIR=${SISDIR}/patches
export LOGDIR=${SISDIR}/logs

source `dirname ${0}`/scripts/Helper.sh

while [[ $# > 0 ]] ; do
	if [ ${1} == "--module" ]; then
		shift 
		export MODULENAME=${1}
    fi
	if [ ${1} == "--prefix" ]; then
		shift 
		export PREFIXDIR=${1}
    fi
	if [ ${1} == "--build" ]; then
		shift 
		export BUILDDIR=${1}
    fi
	if [ ${1} == "--compiler" ]; then
		shift 
		export COMPILERNAME=${1}
    fi
    shift
done

if [ "x${MODULENAME}" == "x" ]; then
	echo "Please specify a module name using --module!"
	exit 1
fi

if [ "x${PREFIXDIR}" == "x" ]; then
	echo "Please specify an installation directory using --prefix!"
	exit 1
fi

if [ "x${BUILDDIR}" == "x" ]; then
	echo "Please specify a build directory using --build!"
	exit 1
fi

if [ "x${COMPILERNAME}" != "xgcc" ] && [ "x${COMPILERNAME}" != "xclang" ]; then
	echo "Compiler \"${COMPILERNAME}\" is not supported! Choose either \"gcc\" or \"clang\" using --compiler!"
	exit 1
fi

mkdir -p ${PREFIXDIR}
cd ${SCRIPTSDIR}

ExecuteAndPipe bashrc 			# more or less sensible stuff for the bashrc -> check script and uncomment if you fancy
ExecuteAndPipe emacs 			# more or less sensible stuff for emacs -> check script and uncomment if you fancy
ExecuteAndPipe yum 				# necessary yum packets 

ExecuteAndPipe modules 			# compile modules if necessary
source ${HOME}/.bashrc 			# load the bashrc and all modules stuff
    
ExecuteAndPipe ${COMPILERNAME} 	# compile a new compiler

ExecuteAndPipe modulefile 		# must be executed at the same time as the compiler compilation, creates new module file
module load ${MODULENAME} 		# load the module file for right environment settings

ExecuteAndPipe m4				# needed for thrift on fc12
ExecuteAndPipe bison			# needed for thrift on fc12

ExecuteAndPipe wireshark		# useful tool for network debugging
ExecuteAndPipe nmap				# useful tool for network scanning

ExecuteAndPipe gdb				# the debugger

ExecuteAndPipe valgrind			# performance profiling
ExecuteAndPipe valkyrie			# GUI for valgrind

ExecuteAndPipe python			# everybody needs python
ExecuteAndPipe pycairo			# stuff for pygtk
ExecuteAndPipe pygobject		# stuff for pygtk
ExecuteAndPipe pygtk			# needed for GUIs

ExecuteAndPipe fftw				# needed for ROOT
ExecuteAndPipe gsl				# needed for ROOT
ExecuteAndPipe cfitsio			# needed for ROOT and CHEC, send from hell :-(

ExecuteAndPipe boost			# awesome C++ libraries

ExecuteAndPipe swig				# python wrapper, requires boost

ExecuteAndPipe zmq				# sockets on steroids
ExecuteAndPipe protobuf			# serialization library
ExecuteAndPipe thrift			# fancy interprocess communication
ExecuteAndPipe omniorb			# old school interprocess communication

ExecuteAndPipe cmake			# needed for ROOT and is awesome anyways

ExecuteAndPipe root				# old ROOT, everybody hates cint :-(

ExecuteAndPipe pip				# useful python packets
ExecuteAndPipe pymysql			# needed for db access

cd

unset SISDIR
unset SCRIPTDIR
unset SOURCEDIR
unset PATCHDIR
unset LOGDIR

unset MODULENAME
unset PREFIXDIR
unset BUILDDIR
unset COMPILERNAME

echo "All done"
